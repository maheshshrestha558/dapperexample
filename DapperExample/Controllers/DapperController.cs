﻿using DapperModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PokemonReviewApp.Repository;
using static Dapper.SqlMapper;

namespace PokemonReviewApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DapperController : ControllerBase
    {
        private readonly DapperRepository _repository;

        public DapperController(DapperRepository dappercurdRepository)
        {
            _repository = dappercurdRepository;
        }
        [HttpGet]
        [Route("GetEmployee")]


        public  IActionResult GetEmployee()
        {
            var entities =  _repository.GetAll();
            return Ok(entities);
        }

        [HttpGet]
        [Route("GetEmployeeById")]

        public IActionResult GetEmployeeById(int id)
        {
            var entity = _repository.GetById(id);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }

        [HttpPost]
        [Route("PostEmployee")]


        public IActionResult PostEmployee([FromBody] Employeeinfo entity)
        {
            _repository.Insert(entity);
            //return CreatedAtAction("Get", new { id = entity.Id }, entity);
            return Ok(entity);
        }

        [HttpPut("{id}")]
        [Route("PutEmployee")]

        public IActionResult PutEmployee(int id, [FromBody] Employeeinfo entity)
        {
            if (id != entity.Id)
                return BadRequest();

            _repository.Update(entity);
            return Ok(entity);

        }

        [HttpDelete("{id}")]
        [Route("DeleteEmployee")]

        public IActionResult DeleteEmployee(int id)
        {
            _repository.Delete(id);
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                return Ok("Data is Deleted");
            }
        }
    }
}
