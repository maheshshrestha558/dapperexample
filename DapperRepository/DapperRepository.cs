﻿using Dapper;
using DapperExample.Services;
using DapperModel;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Data;
using System.Linq;

namespace PokemonReviewApp.Repository
{
    public class DapperRepository
    {
        private readonly SqlConnecntionHelper _connectionString;

        public DapperRepository(SqlConnecntionHelper connectionString)
        {
            _connectionString = connectionString;
        }

        //        private readonly string _connectionString;

        //        public DapperRepository(IConfiguration configuration)
        //        {
        //#pragma warning disable CS8601 // Possible null reference assignment.
        //            _connectionString = configuration.GetConnectionString("DefaultConnection");
        //        }
        public IEnumerable<Employeeinfo> GetAll()

        {
            using var connection = _connectionString.Create();
            connection.Open();
            var result = connection.Query<Employeeinfo>("GetEmployee", commandType: CommandType.StoredProcedure);
            return result;
        }
        public Employeeinfo GetById(int id)
        {
            using var connection = _connectionString.Create();

            connection.Open();
#pragma warning disable CS8603 // Possible null reference return.
            return  connection.QueryFirstOrDefault<Employeeinfo>("GetEmployeeById", new { Id = id },
                                                                commandType: CommandType.StoredProcedure);
        }

        public void Insert(Employeeinfo entity)
        {
            using var connection = _connectionString.Create();

            connection.Open();
            connection.ExecuteAsync("PostEmployee", entity,  commandType: CommandType.StoredProcedure);
        }

        public void Update(Employeeinfo entity)
        {
            using var connection = _connectionString.Create();

            //using var connection = new SqlConnection(_connectionString);
            connection.Open();
            connection.ExecuteAsync("UpdateEmployee", entity, commandType: CommandType.StoredProcedure);
        }

        public void Delete(int id)
        {
            using var connection = _connectionString.Create();

            //using var connection = new SqlConnection(_connectionString);
            connection.Open();
            connection.ExecuteAsync("DeleteEmployee", new { Id = id }, commandType: CommandType.StoredProcedure);
        }

    }
}
