﻿
using Microsoft.Data.SqlClient;

namespace DapperExample.Services
{
    public class SqlConnecntionHelper
    {
        private readonly string _connectionString;

        public SqlConnecntionHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection Create()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
