﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
namespace DapperExample.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class fileController : ControllerBase
    {
        

        [HttpPost]
        [Route("uploadfile")]
    public async Task<IActionResult> uploadfile(IFormFile file,CancellationToken cancellationToken)
        {
            if (file == null)
                return NotFound("file not found");
            await WriteFile(file);
            return Ok("file is uploaded");
        }
        private async Task<bool> WriteFile(IFormFile file)
        {
            bool isSaveSuccess = false;
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = DateTime.Now.Ticks + extension;

                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "upload\\file");

                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }

                var path = Path.Combine(Directory.GetCurrentDirectory(), "upload\\file", fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                isSaveSuccess = true;

            }
            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException();

            }

            return isSaveSuccess;

        }



    }
}
